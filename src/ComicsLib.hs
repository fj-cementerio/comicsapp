{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RankNTypes                #-}
-- {-# OPTIONS -fglasgow-exts #-}
{- |
Module      : ComicsLib
Description : Módulo principal de trabajo para el back de la aplicación
Copyright   : (c) Francisco José Fuentes Rodríguez, 2015
License     : BSD
Maintainer  : moriquendium@gmail.com
Stability   : experimental

Este módulo contiene todas las declaraciones necesarias para el back, desde los
datos básicos hasta las principales estructuras complejas usadas en otras partes.
Se crea con la intención de ser la semilla del "centro" de la aplciación, para poder
ser accedido desde la infraestructura de publicación del API Restful y siendo usado
para definir los accesos a los diferentes servicios externos: base de datos, terceras
APIs para recuperación de datos, etc.

 -}
module ComicsLib
   (
   -- * Identificadores e igualdades
   --Id, Ident, IdDB, IdBusqueda,
   -- * Búsquedas
    Resultado, Resto, Id,
   -- * Datos de negocio
    Ejemplar, Serie
   ) where

import           Data.Time.Calendar (Day)

fix :: (t -> t) -> t
fix f = f (fix f)

-- * Identificadores e igualdades


-- * Búsquedas

-- | Un término de búsqueda: una cade ade caracteres o un entero, según lo que se haya buscado
data TerminoB = String | Int
-- | Un resultado, que será un Identificador y el resto, un personaje, grupo, serie, etc.
data Resultado = Res TerminoB Id Resto
data Resto = Rp PersonajeCompleto | Gr Grupo | Cl Serie | Ej Ejemplar

-- | Id: identificador de un resultado. Depende del tipo de resultado que devuelva, así será el id.
--
-- Un personaje: nombre, versión y editorial
--
-- Un Grupo: nombre y editorial
--
-- Una Serie: nombre, volumen y editorial
--
-- Un Ejemplar determinado: id
data  Id =
  P {
    nP  :: String,
    ver :: String,
    edP :: String
  } |
  G {
    nG  :: String,
    edG :: String
  }|
  S {
    nS  :: String,
    vol :: String,
    ed  :: String
  } |
  E {
    nE::Int
  }
  deriving (Eq, Show)

-- * Datos de negocio

data DataBase= DC {
    creadoPor   :: String,
    nombre      :: String,
    descripcion :: String
  }
  deriving (Show)

-- | Un ejemplar de un comic
data Ejemplar = Edb {
    guionista        :: [String],
    dibujante        :: [String],
    resumen          :: String,
    fechaPublicacion :: Day,
    personajes       :: [PersonajeCompleto]
  }
  deriving (Show)

-- | Una serie de comics cualquiera
data Serie = Sdb {
    dSer       :: DataBase,
    ejemplares :: [Ejemplar]
  }
  deriving (Show)

data PersonajeCompleto = Pc {
    dPer    :: DataBase,
    activo  :: [(Grupo, Ejemplar, Ejemplar)],
    poderes :: String
  }
  deriving (Show)

-- | Un grupo de superhéroes
data Grupo = Gdb {
    dGr     :: DataBase,
    version :: String,
    lEjs    :: [Ejemplar],
    lPers   :: [PersonajeCompleto]
  }
  deriving (Show)
